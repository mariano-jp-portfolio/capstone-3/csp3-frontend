import { Fragment, useState, useEffect } from 'react';
import { Bar } from 'react-chartjs-2';
import moment from 'moment';

// Component import
import Head from '../../components/Head';

// helper
import AppHelper from '../../helpers/app-helper';

// Monthly expenses
export default function MonthlyExpenses() {
	// State
	const [records, setRecords] = useState([]);
	const [months, setMonths] = useState(["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]);
	const [monthlyExpense, setMonthlyExpense] = useState([]);
	
	// Effect
	useEffect(() => {
		fetch(`${AppHelper.API_URL}/users/details`, {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
		})
		.then(res => res.json())
		.then(user => {
			if (typeof user !== 'undefined') {
				setRecords(user.records);
			}
		});
	}, []);
	
	useEffect(() => {
		setMonthlyExpense(months.map(month => {
			let total = 0;
			
			records.forEach(element => {
				if ((moment(element.createdOn).format('MMMM') === month) && element.type === "Expense") {
					total += element.amount;
				}
			});
			
			return total;
		}));
	}, [records]);
	
	const data = {
		labels: months,
		datasets: [{
			label: 'Monthly Expenses for the year 2021',
			backgroundColor: 'rgba(255, 99, 132, 0.2)',
			borderColor: 'rgba(255, 99, 132, 1)',
			borderWidth: 1,
			hoverBackgroundColor: 'rgba(255, 99, 132, 0.4)',
			hoverBorderColor: 'rgba(255, 99, 132, 1)',
			data: monthlyExpense
		}]
	};
	
	const headData = {
		title: 'Monthly Expenses',
		description: 'Take note of how much your expenses are.'
	};
	
	return (
		<Fragment>
			<Head dataProp={headData} />
			<h2>Monthly Expenses</h2>
			<Bar data={ data } />
		</Fragment>	
	);
}