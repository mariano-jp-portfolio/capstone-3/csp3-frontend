import { Fragment, useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// React-Bootstrap
import { Row, Col, Form, Button } from 'react-bootstrap';

// Component import
import Head from '../../components/Head';

// helper
import AppHelper from '../../helpers/app-helper';

// SweetAlert2 
import Swal from 'sweetalert2';

// create new record page
export default function CreateRecord() {
	const headData = {
		title: 'Create A New Record',
		description: 'Record-keeping is important in budget tracking.'
	}
	
	return (
		<Fragment>
			<Head dataProp={headData} />
			<Row className="justify-content-center">
				<Col xs md="6">
					<NewRecord />
				</Col>
			</Row>
		</Fragment>
	);
}

// Form
const NewRecord = () => {
	// router
	const router = useRouter();
	
	// State
	const [category, setCategory] = useState([]);
	const [type, setType] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [amount, setAmount] = useState(0);
	const [isActive, setIsActive] = useState(false);
	
	// function to handle the submitted form
	function createRecord(e) {
		e.preventDefault();
		
		fetch(`${AppHelper.API_URL}/users/add-record`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
			},
			body: JSON.stringify({
				type: type,
				name: name,
				description: description,
				amount: amount
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {
				setType('');
				setName('');
				setDescription('');
				setAmount(0);
				
				Swal.fire('Success!', 'Congrats, you\'re one step closer to managing your finances.', 'success');
				
				// redirect
				router.push('/records');
			} else {
				Swal.fire('Oops...', 'Something went wrong. Please try again.', 'error');
			}
		})
	}

	// Effect
	useEffect(() => {
		if (description !== '' && amount > 0) {
			setIsActive(true);
		}
	}, [description, amount]);
	
	// useEffect to fetch all records based on their Category Types
	useEffect(() => {		
		fetch(`${AppHelper.API_URL}/users/details`, {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
		})
		.then(res => res.json())
		.then(user => {
			// type State
			if (type === "Expense") {
				const categoryArr = user.categories.map(category => {
					// category type
					if (category.type === "Expense") {
						return (
							<option key={category._id} value={category.name}>{category.name}</option>
						);
					}
				});
				
				setCategory(categoryArr);
			} else if (type === "Income") {
				const categoryArr = user.categories.map(category => {
					if (category.type === "Income") {
						return (
							<option key={category._id} value={category.name}>{category.name}</option>
						);
					}
				});
				
				setCategory(categoryArr);
			}
		})
	}, [type]);

	return (
		<Fragment>
			<h2>Create A New Record</h2>
			<Form onSubmit={ createRecord }>
				<Form.Group controlId="categoryType">
					<Form.Label>Category Type:</Form.Label>
					<Form.Control
						as="select"
						value={ type }
						onChange={(e) => setType(e.target.value)}
						required
					>
						<option disabled value="">Select a category</option>
						<option value="Expense">Expense</option>
						<option value="Income">Income</option>
					</Form.Control>
				</Form.Group>
				
				<Form.Group controlId="categoryName">
					<Form.Label>Category Name:</Form.Label>
					<Form.Control
						as="select"
						value={ name }
						onChange={(e) => setName(e.target.value)}
						required
					>
						<option disabled value="">Select a name</option>
						{ category }
					</Form.Control>
				</Form.Group>
				
				<Form.Group controlId="recordDescription">
					<Form.Label>Description:</Form.Label>
					<Form.Control
						type="text"
						placeholder="Enter short description"
						value={ description }
						onChange={(e) => setDescription(e.target.value)}
						required
					/>
				</Form.Group>
				
				<Form.Group controlId="recordAmount">
					<Form.Label>Amount:</Form.Label>
					<Form.Control
						type="number"
						placeholder="Enter amount"
						value={ amount }
						onChange={(e) => setAmount(e.target.value)}
						required
					/>
				</Form.Group>
				
				{ isActive ?
					<Button className="mb-3 btn btn-block" variant="primary" type="submit">
						Submit
					</Button>
					:
					<Button disabled className="mb-3 btn btn-block" variant="secondary">
						Submit
					</Button>
				}
				
				<Button href="/records" className="mb-3 btn btn-block" variant="dark">
					Back
				</Button>
			</Form>
		</Fragment>
	);
};