import { useState, useEffect } from 'react';
import { Pie } from 'react-chartjs-2';

// helper
import { colorRandomizer } from '../helpers/colorRandomizer';

export default function PieChart({ recordData }) {
	// State
	const [name, setName] = useState([]);
	const [amount, setAmount] = useState([]);
	const [bgColors, setBgColors] = useState([]);
	
	// Effect
	useEffect(() => {
		if (recordData.length > 0) {
			setName(recordData.map(element => element.name));
			setAmount(recordData.map(element => element.amount));
			setBgColors(recordData.map(() => `#${ colorRandomizer() }`));
		}
	}, [recordData]);
	
	const data = {
		labels: name,
		datasets: [{
			label: 'Budget Allocation',
			data: amount,
			backgroundColor: bgColors,
			borderColor: bgColors,
			hoverBackgroundColor: bgColors,
			radius: "80%"
		}]
	};
	
	return (
		<Pie data={ data } />
	);
}