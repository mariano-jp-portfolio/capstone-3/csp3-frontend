import React, { Fragment, useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';

// React-Bootstrap
import { Navbar, Nav } from 'react-bootstrap';

// Navbar won't show links to different pages except for the login and register
// If user logs in, all links to other pages will show and the Navbar.Brand will be enabled to redirect to user's profile. For now, the brand will just redirect to the categories page. The log out link will also be available
export default function NaviBar() {
	const { user } = useContext(UserContext);
	
	const [isActive, setIsActive] = useState(false);
	
	useEffect(() => {
		if (user.id !== null) {
			setIsActive(true);
		}
	}, [user.id]);
	
	return (
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
			{ isActive ?
				<Navbar.Brand href="/categories">JP's</Navbar.Brand>
				:
				<Navbar.Brand href="#">JP's</Navbar.Brand>
			}
			<Navbar.Toggle aria-controls="responsive-navbar-nav" />
			<Navbar.Collapse id="responsive-navbar-nav">
				
					{ user.id !== null ?
						<Fragment>
							<Nav className="mr-auto">
								<Nav.Link href="/categories">Categories</Nav.Link>
								<Nav.Link href="/records">Records</Nav.Link>
								<Nav.Link href="/expenses">Monthly Expense</Nav.Link>
								<Nav.Link href="/income">Monthly Income</Nav.Link>
								<Nav.Link href="/trend">Trend</Nav.Link>
								<Nav.Link href="/breakdown">Breakdown</Nav.Link>
							</Nav>
							
							<Nav>
								<Nav.Link href="/logout">Log Out</Nav.Link>
							</Nav>
						</Fragment>
						:
						<Nav className="ml-auto">
							<Nav.Link href="/register">Register</Nav.Link>
							<Nav.Link href="/">Login</Nav.Link>
						</Nav>
					}
			</Navbar.Collapse>
		</Navbar>
	);
}